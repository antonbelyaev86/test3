<?php
namespace Skyeng\Providers\Data\Decorators;

use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LoggerInterface;
use Skyeng\Providers\Data\DataProviderInterface;

class DataCachingDecorator implements DataProviderInterface
{
    /**
     * @var DataProviderInterface $provider
     */
    private  $provider;
    /**
     * @var CacheItemPoolInterface
     */
    private $cache;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param DataProviderInterface $provider
     * @param LoggerInterface $logger
     * @param CacheItemPoolInterface $cache
     */
    public function __construct(
        DataProviderInterface $provider,
        LoggerInterface $logger,
        CacheItemPoolInterface $cache
    ) {
        $this->provider = $provider;
        $this->logger = $logger;
        $this->cache = $cache;
    }

    /**
     * {@inheritdoc}
     */
    public function get(array $request): array
    {
        try {
            $cacheKey = $this->getCacheKey($request);
            $cacheItem = $this->cache->getItem($cacheKey);
            if ($cacheItem->isHit()) {
                return $cacheItem->get();
            }

            $result = $this->provider->get($request);

            $cacheItem
                ->set($result)
                ->expiresAt(
                    (new DateTime())->modify('+1 day')
                );

            return $result;
        } catch (\Exception $e) {
            $this->logger->critical(__CLASS__.' '.__METHOD__.' fail: '.$e->getMessage());
        }

        return [];
    }

    public function getCacheKey(array $request): string
    {
        return md5((string)json_encode($request, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
    }
}