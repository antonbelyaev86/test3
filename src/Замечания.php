<?php

namespace src\Integration;


/**
 *
 * Class DataProvider
 * @package src\Integration
 */
class DataProvider
{
    private $host;
    private $user;
    private $password;

    /**
     * @param $host
     * @param $user
     * @param $password
     */
    public function __construct($host, $user, $password)
    {
        $this->host = $host;
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * @param array $request
     *
     * @return array
     */
    public function get(array $request)
    {
        // returns a response from external service
    }
}


namespace src\Decorator;

use DateTime;
use Exception;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LoggerInterface;
use src\Integration\DataProvider;


/**
 * 1. Не правильно реализован паттерн декоратор, в сам декоратор нужно пробрасывать декорируемый объект
 * и декратор с объектом должны реализовывать один интерфейс
 * 2. Свойства $cache и $logger должны быть private
 * 3. Объекты CacheItemPoolInterface и LoggerInterface должны передаваться в конструкторе
 * 4. Не проставлены возвращаемые типы у методов
 * 5. Не информативныая запись в лог
 * 6. Неверное использование inheritdoc и не у всех методов есть описание phpDoc
 * 7. Не известно что в массиве $input метода getCacheKey, лучше обернуть в md5
 *
 * Class DecoratorManager
 * @package src\Decorator
 */
class DecoratorManager extends DataProvider
{
    public $cache;
    public $logger;

    /**
     * @param string $host
     * @param string $user
     * @param string $password
     * @param CacheItemPoolInterface $cache
     */
    public function __construct($host, $user, $password, CacheItemPoolInterface $cache)
    {
        parent::__construct($host, $user, $password);
        $this->cache = $cache;
    }

    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function getResponse(array $input)
    {
        try {
            $cacheKey = $this->getCacheKey($input);
            $cacheItem = $this->cache->getItem($cacheKey);
            if ($cacheItem->isHit()) {
                return $cacheItem->get();
            }

            $result = parent::get($input);

            $cacheItem
                ->set($result)
                ->expiresAt(
                    (new DateTime())->modify('+1 day')
                );

            return $result;
        } catch (Exception $e) {
            $this->logger->critical('Error');
        }

        return [];
    }

    public function getCacheKey(array $input)
    {
        return json_encode($input);
    }
}

